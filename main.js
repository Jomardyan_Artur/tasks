function deepClone(input) {

    if (typeof input !== "object" || input === null) {
        return input;
    } else {
        if (input instanceof Array) {
            let arrCopy = [];
            input.forEach((element) => {
                if (element instanceof Array || element instanceof Object) {
                     arrCopy.push(deepClone(element));
                }else {
                    arrCopy.push(element)
                }
            });
            return arrCopy
        }
        if (input instanceof Object) {
            let objCopy = {};
            for (let i in input) {
                if (input[i] instanceof Array || input[i] instanceof Object) {
                    objCopy[i] = deepClone(input[i])
                }else {
                    objCopy[i] = input[i]
                }
            }
            return objCopy
        }
    }
}

//------------------------------------------------------

// test for arr

// let arr = [5, 9, [15, [2121,[{name:"Text"}]]], {name: {aaaa:"Text"}, y: [999]}]
//
// let arrCopy = deepClone(arr);
//
// arrCopy[2][1][1][0].name = "Changed";
// arrCopy[3].name.aaaa = "changed"
// arrCopy[3].y[0] = "changed"
// console.log("Original Array >>> ", arr)
// console.log("Copy Array >>> ", arrCopy)

//------------------------------------------------------

// test for obj

// let obj = {
//     q: [5,{name:"Text"}],
//     name: {d: 9,arr:[555]}
// }

// let objClone  = deepClone(obj)
// objClone.q[1].name = "changed";
// objClone.name.arr[0] = "changed"
// console.log("Original Object >>> ",obj)
// console.log("Copy Object",objClone)
